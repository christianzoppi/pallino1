flash.vue(function() {

	if(!document.querySelector('#news-listing-live')) return;

	new Vue({
		el: '#news-listing-live',
		data: {
			loading: true,
			stories: [],
			page: 1,
			pages: 0,
			limit: 1,
			category: 'all'
		},
		methods: {
			fetchStories: function() {
				var self = this;
				this.loading = true;

				var endpoint = 'https://vc6hn2s7g5.execute-api.eu-west-1.amazonaws.com/default/flashsearch';
				var conditions = [{
		            field: 'component',
		            operator: 'in',
		            value: 'News Module'
		        }];
		        if(this.category !== 'all') {
		        	conditions.push({
		        		field: 'category',
		        		operator: 'in',
		        		value: this.category
		        	});
		        }
				var data = {
				    version: 'draft', // optional, it can be published or draft,
				    token: st, // base64 encoded storyblok token,
				    sort_by: 'position:asc', // optional, default to position:asc
				    page: this.page, //optional, default to all of the pages
				    per_page: this.limit, // default to 100
				    conditions: conditions
				};

				var xhr = new XMLHttpRequest();
				xhr.open('POST', endpoint, true);
				xhr.send(param(data));
				xhr.onload = function() {
				    var data = JSON.parse(this.responseText);
				    self.stories = data.stories;
				    self.pages = Math.ceil(data.total / self.limit);
				    self.loading = false;
				}
			},
			changePage: function(page) {
			  	window.scrollTo(0, this.$el.offsetTop);
				this.page = page;
				this.fetchStories();
			},
			changeCategory: function() {
				this.page = 1;
				this.fetchStories();
			},
			imgix: function(url) {
				return url.replace('https://s3.amazonaws.com/','//').replace('//a.storyblok.com/f/', 'https://togetherdigital.imgix.net/') + '?q=60&w=500&lossless=1&fm=pjpg&ixlib=js-1.1.1&s=66267053a7df3214efbf336efa1f0c0c';
			}
		},
		mounted: function() {
			this.fetchStories();
		}

	});

});